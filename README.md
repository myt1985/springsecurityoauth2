# springsecurityoauth2

OAuth2.0 demo developed by Spring Security.



## basic-oauth2
最基础的oauth2示例，完全使用SpringSecurity最基础的实现方式。



## oauth2-2
单体服务版的示例，自定义了登录、授权页面。



## authorization-server
分布式授权服务器。



## resource-server
分布式资源服务器。



## net5ijy-oauth2-common
通用类。



## oauth2-httpclient
使用httpclient调用资源服务器接口的示例程序。



# 参考资料

spring-boot RestTemplate 连接池

https://www.cnblogs.com/zhangzhi19861216/p/8594674.html



spring boot使用RestTemplate+httpclient连接池发送http消息

https://www.cnblogs.com/coderjinjian/p/9644923.html



spring boot集成restTemplate，http连接池

https://blog.csdn.net/qq_32302897/article/details/84550377



RestTemplate实践（及遇到的问题）

https://www.cnblogs.com/duanxz/p/3510622.html



Spring Security OAuth2 开发指南

https://www.cnblogs.com/xingxueliao/p/5911292.html



