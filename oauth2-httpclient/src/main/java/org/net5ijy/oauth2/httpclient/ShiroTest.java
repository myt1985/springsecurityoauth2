package org.net5ijy.oauth2.httpclient;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.config.IniSecurityManagerFactory;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.subject.Subject;

public class ShiroTest {

  public static void main(String[] args) {

    IniSecurityManagerFactory factory = new IniSecurityManagerFactory("classpath:shiro.ini");

    SecurityManager securityManager = factory.getInstance();

    SecurityUtils.setSecurityManager(securityManager);

    Subject user = SecurityUtils.getSubject();

    boolean authenticated = user.isAuthenticated();

    System.out.println(authenticated);

    UsernamePasswordToken token = new UsernamePasswordToken("admin", "secret");

    user.login(token);

    authenticated = user.isAuthenticated();

    System.out.println(authenticated);
  }
}
