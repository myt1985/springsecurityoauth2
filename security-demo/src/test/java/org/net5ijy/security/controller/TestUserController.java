package org.net5ijy.security.controller;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.net5ijy.security.config.AppConfig;
import org.net5ijy.security.util.RequestUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { AppConfig.class })
@WebAppConfiguration
@Rollback
@Transactional
public class TestUserController {

	@Autowired
	private WebApplicationContext context;

	private MockMvc mockMvc;

	@Before
	public void before() {
		// 获取mockmvc对象
		mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
	}

	@Test
	public void testDeleteUser() throws Exception {
		mockMvc.perform(
				MockMvcRequestBuilders
						.post("/user/delete/113")
						.accept(MediaType.APPLICATION_JSON_UTF8)
						.param("id", "1")
						.header(RequestUtil.X_REQUESTED_WITH_HEADER,
								RequestUtil.XML_HTTP_REQUEST))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.jsonPath("$.code").value(0));
	}

	@Test
	public void testDeleteUserNotFound() throws Exception {
		mockMvc.perform(
				MockMvcRequestBuilders
						.post("/user/delete/100000000")
						.accept(MediaType.APPLICATION_JSON_UTF8)
						.header(RequestUtil.X_REQUESTED_WITH_HEADER,
								RequestUtil.XML_HTTP_REQUEST))
				.andExpect(
						MockMvcResultMatchers.status().is(
								HttpServletResponse.SC_INTERNAL_SERVER_ERROR))
				.andExpect(MockMvcResultMatchers.jsonPath("$.code").value(99))
				.andDo(MockMvcResultHandlers.print());
	}

	@Test
	public void testDeleteUserNotFoundWithoutAjax() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post("/user/delete/100000000"))
				.andExpect(
						MockMvcResultMatchers.status().is(
								HttpServletResponse.SC_INTERNAL_SERVER_ERROR))
				.andExpect(MockMvcResultMatchers.forwardedUrl("/error50x"))
				.andDo(MockMvcResultHandlers.print());
	}
}
