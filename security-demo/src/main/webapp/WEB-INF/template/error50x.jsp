<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	String contextPath = application.getContextPath();
%>
<!DOCTYPE html>
<html lang="zh_cn">
<head>
	<meta charset="UTF-8">
	<meta name="_basePath" content="<%=contextPath %>">
	<title>用户管理</title>

	<link rel="shortcut icon" href="<%=contextPath %>/static/favicon.ico" type="image/x-icon" />

	<script type="text/javascript" src="<%=contextPath %>/static/js/jquery-1.9.1.min.js"></script>

	<script type="text/javascript" src="<%=contextPath %>/static/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="<%=contextPath %>/static/css/bootstrap.min.css" />
	<link rel="stylesheet" href="<%=contextPath %>/static/css/bootstrap-theme.min.css" />

	<link rel="stylesheet" href="<%=contextPath %>/static/css/style.css" />

	<script type="text/javascript" src="<%=contextPath %>/static/js/user.js"></script>

</head>
<body>
<%-- 引入头部 --%>
<jsp:include page="include/header.jsp"></jsp:include>
<div id="main" class="container">
	<span><strong>Sorry</strong>, an unexpected error occurred on the server.</span> 
	<a href="javascript:history.go(-1);">Go back</a> after <span id="timmer">5</span> seconds.
	<br /><br />
	错误信息：${exception }
</div>
<%-- 引入尾部 --%>
<jsp:include page="include/footer.jsp"></jsp:include>
</body>
</html>
