<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
	String contextPath = application.getContextPath();
%>
<!DOCTYPE html>
<html lang="zh_cn">
<head>
	<meta charset="UTF-8">
	<meta name="_basePath" content="<%=contextPath %>">
	<title>用户管理</title>

	<link rel="shortcut icon" href="<%=contextPath %>/static/favicon.ico" type="image/x-icon" />

	<script type="text/javascript" src="<%=contextPath %>/static/js/jquery-1.9.1.min.js"></script>

	<script type="text/javascript" src="<%=contextPath %>/static/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="<%=contextPath %>/static/css/bootstrap.min.css" />
	<link rel="stylesheet" href="<%=contextPath %>/static/css/bootstrap-theme.min.css" />

	<script type="text/javascript" src="<%=contextPath %>/static/js/Validform_v5.3.2_ncr_min.js"></script>
	<link rel="stylesheet" href="<%=contextPath %>/static/css/Validform.css" />

	<script type="text/javascript" src="<%=contextPath %>/static/js/toastr.min.js"></script>
	<link rel="stylesheet" href="<%=contextPath %>/static/css/toastr.min.css" />

	<script type="text/javascript" src="<%=contextPath %>/static/js/layui/layui.all.js"></script>
	<link rel="stylesheet" href="<%=contextPath %>/static/js/layui/css/layui.css" />

	<link rel="stylesheet" href="<%=contextPath %>/static/css/style.css" />

	<script type="text/javascript" src="<%=contextPath %>/static/js/user.js"></script>

</head>
<body>
<%-- 引入头部 --%>
<jsp:include page="include/header.jsp"></jsp:include>
<div id="main" class="container">
	<!-- 用户名密码登录 -->
	<form id="username_form" class="form-horizontal login_form" action="login" method="post">
		<input type="hidden" name="_type" value="img" />
		<div class="form-group">
			<label class="col-sm-1 control-label">用&ensp;户&ensp;名: </label>
			<div class="col-sm-5">
				<input class="form-control" placeholder="用户名" name="username" 
					datatype="/[a-zA-Z_0-9]{6,20}/g" sucmsg="用户名验证通过！" nullmsg="请填写用户名！" errormsg="请填写用户名！" />
			</div>
			<div class="col-sm-6 control-label" style="text-align: left"></div>
		</div>
		<div class="form-group">
			<label class="col-sm-1 control-label">密&#12288;&#12288;码: </label>
			<div class="col-sm-5">
				<input class="form-control" placeholder="请填写密码" type="password" name="password" 
					datatype="/[a-zA-Z_0-9]{6,20}/g" sucmsg="密码验证通过！" nullmsg="请填写密码！" errormsg="请填写密码！" />
			</div>
			<div class="col-sm-6 control-label" style="text-align: left"></div>
		</div>
		<div class="form-group">
			<label class="col-sm-1 control-label">验&ensp;证&ensp;码: </label>
			<div class="col-sm-3">
				<input class="form-control col-sm-3" placeholder="请填写验证码" name="img_code"
					datatype="/^[A-Za-z0-9]{4,}$/g" sucmsg="" nullmsg="请填写验证码！" errormsg="请填写验证码！" />
			</div>
			<div class="col-sm-3">
				<img alt="验证码" src="<%=contextPath %>/vCode?type=img&_d="
				 	onclick="this.src='<%=contextPath %>/vCode?type=img&_d=' + (new Date()).getTime()"
				 	style="border: 1px solid #e3e3e3;" />
			</div>
			<div class="col-sm-6 control-label" style="text-align: left"></div>
		</div>
		<div class="form-group">
			<label class="col-sm-1 control-label"></label>
			<div class="col-sm-4 checkbox">
				<label><input type="checkbox" name="remember-me">记住我</label>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-1 col-sm-10">
				<button type="submit" class="btn btn-default"> 登 录 </button>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-1 col-sm-10">
				<a class="login-link" href="javascript:void(0);">立即注册</a>
				&nbsp;&nbsp;
				<a class="login-link change-type" href="javascript:void(0);">手机登录</a>
			</div>
		</div>
		<%-- CSRF --%>
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	</form>
	<!-- 手机登录 -->
	<form id="mobile_form" class="form-horizontal login_form" action="mobile_login" method="post" style="display: none;">
		<input type="hidden" name="_type" value="sms" />
		<div class="form-group">
			<label class="col-sm-1 control-label">手&ensp;机&ensp;号: </label>
			<div class="col-sm-3">
				<input class="form-control col-sm-3" placeholder="请填写手机号" name="mobile"
					datatype="m" sucmsg="手机号码验证通过！" nullmsg="请填写手机号" errormsg="请填写手机号" />
			</div>
			<div class="col-sm-3">
				<a id="send_sms" class="btn btn-info" href="javascript:void(0);" role="button"><span class="glyphicon glyphicon-send" aria-hidden="true"></span> 发 送 </a>
			</div>
			<div class="col-sm-6 control-label" style="text-align: left"></div>
		</div>
		<div class="form-group">
			<label class="col-sm-1 control-label">验&ensp;证&ensp;码: </label>
			<div class="col-sm-3">
				<input class="form-control col-sm-3" placeholder="请填写验证码" name="sms_code"
					datatype="/^[A-Za-z0-9]{4,}$/g" sucmsg="" nullmsg="请填写验证码！" errormsg="请填写验证码！" />
			</div>
			<div class="col-sm-6 control-label" style="text-align: left"></div>
		</div>
		<div class="form-group">
			<label class="col-sm-1 control-label"></label>
			<div class="col-sm-4 checkbox">
				<label><input type="checkbox" name="remember-me">记住我</label>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-1 col-sm-10">
				<button type="submit" class="btn btn-default"> 登 录 </button>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-1 col-sm-10">
				<a class="login-link" href="javascript:void(0);">立即注册</a>
				&nbsp;&nbsp;
				<a class="login-link change-type" href="javascript:void(0);">用户名登录</a>
			</div>
		</div>
		<%-- CSRF --%>
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	</form>
</div>
<c:if test="${errorMsg != null }">
<input type="hidden" id="error" value="${errorMsg }" />
</c:if>
<%-- 引入尾部 --%>
<jsp:include page="include/footer.jsp"></jsp:include>
</body>
</html>
