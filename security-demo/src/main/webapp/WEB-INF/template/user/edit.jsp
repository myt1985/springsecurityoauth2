<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="/functions" prefix="fns" %>
<%
	String contextPath = application.getContextPath();
%>
<!DOCTYPE html>
<html lang="zh_cn">
<head>

	<meta charset="UTF-8">
	<meta name="_basePath" content="<%=contextPath %>">
	<title>用户管理</title>

	<link rel="shortcut icon" href="<%=contextPath %>/static/favicon.ico" type="image/x-icon" />

	<script type="text/javascript" src="<%=contextPath %>/static/js/jquery-1.9.1.min.js"></script>

	<script type="text/javascript" src="<%=contextPath %>/static/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="<%=contextPath %>/static/css/bootstrap.min.css" />
	<link rel="stylesheet" href="<%=contextPath %>/static/css/bootstrap-theme.min.css" />

	<script type="text/javascript" src="<%=contextPath %>/static/js/laydate/laydate.js"></script>

	<script type="text/javascript" src="<%=contextPath %>/static/js/Validform_v5.3.2_ncr_min.js"></script>
	<link rel="stylesheet" href="<%=contextPath %>/static/css/Validform.css" />

	<script type="text/javascript" src="<%=contextPath %>/static/js/toastr.min.js"></script>
	<link rel="stylesheet" href="<%=contextPath %>/static/css/toastr.min.css" />

	<link rel="stylesheet" href="<%=contextPath %>/static/css/style.css" />

	<script type="text/javascript" src="<%=contextPath %>/static/js/user.js"></script>
</head>
<body>
<%-- 引入头部 --%>
<jsp:include page="../include/header.jsp"></jsp:include>
<div id="main" class="container">
	<a class="btn btn-info" href="<%=contextPath %>/user/users" role="button"> 返 回 </a>
	<br /><br />
	<form class="form-horizontal user_form" action="<%=contextPath %>/user/edit" method="post">
		<input type="hidden" name="id" value="${user.id }" />
		<div class="form-group">
			<label class="col-sm-1 control-label">用&ensp;户&ensp;名: </label>
			<div class="col-sm-5">
				<input class="form-control" name="username" placeholder="请填写用户名，字母、数字、下划线的组合" value="${user.username }"
					datatype="/[a-zA-Z_0-9]{6,20}/g" sucmsg="用户名验证通过！" 
					nullmsg="请填写用户名，字母、数字、下划线的组合！" errormsg="请填写用户名，字母、数字、下划线的组合！" />
			</div>
			<div class="col-sm-6 control-label" style="text-align: left"></div>
		</div>
		<div class="form-group">
			<label class="col-sm-1 control-label">密&#12288;&#12288;码: </label>
			<div class="col-sm-5">
				<input class="form-control" type="password" name="password" placeholder="请填写密码，字母、数字、下划线的组合" value="${user.password }"
					datatype="/[a-zA-Z_0-9]{6,20}/g" sucmsg="密码验证通过！" 
					nullmsg="请填写密码，字母、数字、下划线的组合！" errormsg="请填写密码，字母、数字、下划线的组合！" />
			</div>
			<div class="col-sm-6 control-label" style="text-align: left"></div>
		</div>
		<div class="form-group">
			<label class="col-sm-1 control-label">手&ensp;机&ensp;号: </label>
			<div class="col-sm-5">
				<input class="form-control" name="mobile" placeholder="请填写手机号码" value="${user.mobile }"
					datatype="m" sucmsg="手机号码验证通过！" nullmsg="请填写手机号码！" errormsg="请填写手机号码！" />
			</div>
			<div class="col-sm-6 control-label" style="text-align: left"></div>
		</div>
		<div class="form-group">
			<label class="col-sm-1 control-label">邮&#12288;&#12288;箱: </label>
			<div class="col-sm-5">
				<input class="form-control" name="email" placeholder="请填写邮箱" value="${user.email }"
					datatype="e" sucmsg="邮箱验证通过！" nullmsg="请填写邮箱！" errormsg="请填写邮箱！" />
			</div>
			<div class="col-sm-6 control-label" style="text-align: left"></div>
		</div>
		<div class="form-group">
			<label class="col-sm-1 control-label">生&#12288;&#12288;日: </label>
			<div class="col-sm-5">
				<input class="form-control" id="birthday" name="birthday" placeholder="yyyy-MM-dd" readonly="readonly" 
					value='<fmt:formatDate value="${user.birthday }" pattern="yyyy-MM-dd" />' 
					datatype="*" sucmsg="生日验证通过！" nullmsg="请填写生日！" errormsg="请填写生日！" />
			</div>
			<div class="col-sm-6 control-label" style="text-align: left"></div>
		</div>
		<div class="form-group">
			<label class="col-sm-1 control-label">加入时间: </label>
			<div class="col-sm-5">
				<input class="form-control" id="joinTime" name="joinTime" placeholder="yyyy-MM-dd HH:mm:ss" readonly="readonly" 
					value='<fmt:formatDate value="${user.joinTime }" pattern="yyyy-MM-dd HH:mm:ss" />' 
					datatype="*" sucmsg="加入时间验证通过！" nullmsg="请填写加入时间！" errormsg="请填写加入时间！" />
			</div>
			<div class="col-sm-6 control-label" style="text-align: left"></div>
		</div>
		<!-- 用户角色 -->
		<div class="form-group">
			<label class="col-sm-1 control-label">角&#12288;&#12288;色: </label>
			<div class="col-sm-5 checkbox">
				<c:forEach items="${roles }" var="role">
					<label>
						<input type="checkbox" name="roleIds" value="${role.id }"
							<c:if test="${fns:contains(roleIds, role.id)}">checked</c:if>> ${role.roleName }
					</label>
					&nbsp;
				</c:forEach>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-1 col-sm-10">
				<button type="submit" class="btn btn-default"> 提 交 </button>
			</div>
		</div>
		<%-- CSRF --%>
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	</form>
</div>
<c:if test="${errorMsg != null }">
<input type="hidden" id="error" value="${errorMsg }" />
</c:if>
<%-- 引入尾部 --%>
<jsp:include page="../include/footer.jsp"></jsp:include>
</body>
</html>
