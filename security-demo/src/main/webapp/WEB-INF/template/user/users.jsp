<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%
	String contextPath = application.getContextPath();
%>
<!DOCTYPE html>
<html lang="zh_cn">
<head>

	<meta charset="UTF-8">
	<meta name="_basePath" content="<%=contextPath %>">
	<meta name="_csrf" content="${_csrf.token}"></meta>
	<meta name="_csrf_header" content="${_csrf.headerName}"></meta>
	<title>用户管理</title>

	<link rel="shortcut icon" href="<%=contextPath %>/static/favicon.ico" type="image/x-icon" />

	<script type="text/javascript" src="<%=contextPath %>/static/js/jquery-1.9.1.min.js"></script>

	<script type="text/javascript" src="<%=contextPath %>/static/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="<%=contextPath %>/static/css/bootstrap.min.css" />
	<link rel="stylesheet" href="<%=contextPath %>/static/css/bootstrap-theme.min.css" />

	<script type="text/javascript" src="<%=contextPath %>/static/js/toastr.min.js"></script>
	<link rel="stylesheet" href="<%=contextPath %>/static/css/toastr.min.css" />

	<script type="text/javascript" src="<%=contextPath %>/static/js/layui/layui.all.js"></script>
	<link rel="stylesheet" href="<%=contextPath %>/static/js/layui/css/layui.css" />

	<link rel="stylesheet" href="<%=contextPath %>/static/css/style.css" />

	<script type="text/javascript" src="<%=contextPath %>/static/js/user.js"></script>
</head>
<body>
<%-- 引入头部 --%>
<jsp:include page="../include/header.jsp"></jsp:include>
<div id="main" class="container">
	<a class="btn btn-info" href="<%=contextPath %>/user/add" role="button"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> 添 加 </a>
	<br /><br />
	<table class="table table-bordered table-hover">
		<tr>
			<th>#</th>
			<th>Username</th>
			<th>Mobile</th>
			<th>Email</th>
			<th>Join Time</th>
			<th>Birthday</th>
			<th>Create Time</th>
			<th>State</th>
			<th>Operation</th>
		</tr>
		<c:forEach items="${userResult.rows }" var="u">
			<tr>
				<td>${u.id }</td>
				<td>${u.username }</td>
				<td>${u.mobile }</td>
				<td>${u.email }</td>
				<td>
					<fmt:formatDate value="${u.joinTime }" pattern="yyyy-MM-dd HH:mm:ss" />
				</td>
				<td>${u.birthday }</td>
				<td>
					<fmt:formatDate value="${u.createTime }" pattern="yyyy-MM-dd HH:mm:ss" />
				</td>
				<td>${u.state }</td>
				<td>
					<a class="btn btn-warning btn-sm" href="<%=contextPath %>/user/edit/${u.id}" role="button">
						<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
						 修 改 
					</a>
					<button class="btn btn-danger btn-sm" role="button" style="outline: none;" id="del_${u.id}">
						<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
						 删 除 
					</button>
				</td>
			</tr>
		</c:forEach>
	</table>
	<jsp:include page="../include/pagination.jsp"></jsp:include>
</div>
<%-- 引入尾部 --%>
<jsp:include page="../include/footer.jsp"></jsp:include>
</body>
</html>
