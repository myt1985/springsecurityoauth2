<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
	String path = application.getContextPath();
%>
<div class="row container">
	<div class="col-sm-1 form-control" style="width: 6%;border: 0;box-shadow: none;font-weight: bold;margin-left: 0;padding-left: 0;margin-right: 0;padding-right: 0;">
		每页显示
	</div>
	<div class="col-sm-1" style="width: 6%;margin-left: 0;padding-left: 0;padding-right: 0;">
		<select class="form-control" id="change-size">
			<option value="5" <c:if test="${requestScope.size == 5 }">selected</c:if>>5</option>
			<option value="10" <c:if test="${requestScope.size == 10 }">selected</c:if>>10</option>
			<option value="15" <c:if test="${requestScope.size == 15 }">selected</c:if>>15</option>
			<option value="20" <c:if test="${requestScope.size == 20 }">selected</c:if>>20</option>
			<option value="30" <c:if test="${requestScope.size == 30 }">selected</c:if>>30</option>
		</select>
	</div>
	<div class="col-sm-6">
		<nav aria-label="Page navigation">
			<ul class="pagination" style="margin: 0;">
				<c:choose>
					<c:when test="${userResult.pageNum == 1 }">
						<li class="disabled">
							<a href="javascript:void(0);" aria-label="首页">
								<span aria-hidden="true">&laquo;</span>
							</a>
						</li>
					</c:when>
					<c:otherwise>
						<li>
							<a href="javascript:toPage(1);" aria-label="首页">
								<span aria-hidden="true">&laquo;</span>
							</a>
						</li>
					</c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${userResult.pageNum == 1 }">
						<li class="disabled">
							<a href="javascript:void(0);" aria-label="上一页">
								<span aria-hidden="true">&lsaquo;</span>
							</a>
						</li>
					</c:when>
					<c:otherwise>
						<li>
							<a href="javascript:toPage(${userResult.pageNum - 1 });" aria-label="上一页">
								<span aria-hidden="true">&lsaquo;</span>
							</a>
						</li>
					</c:otherwise>
				</c:choose>
				<c:forEach begin="${userResult.startPage }" end="${userResult.endPage }" var="p">
					<c:choose>
						<c:when test="${p == userResult.pageNum }">
							<li class="active"><a href="javascript:void(0);">${p }</a></li>
						</c:when>
						<c:otherwise>
							<li><a href="javascript:toPage(${p });">${p }</a></li>
						</c:otherwise>
					</c:choose>
				</c:forEach>
				<c:choose>
					<c:when test="${userResult.pageNum == userResult.pageCount }">
						<li class="disabled">
							<a href="javascript:void(0);" aria-label="下一页">
								<span aria-hidden="true">&rsaquo;</span>
							</a>
						</li>
					</c:when>
					<c:otherwise>
						<li>
							<a href="javascript:toPage(${userResult.pageNum + 1 });" aria-label="下一页">
								<span aria-hidden="true">&rsaquo;</span>
							</a>
						</li>
					</c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${userResult.pageNum == userResult.pageCount }">
						<li class="disabled">
							<a href="javascript:void(0);" aria-label="尾页">
								<span aria-hidden="true">&raquo;</span>
							</a>
						</li>
					</c:when>
					<c:otherwise>
						<li>
							<a href="javascript:toPage(${userResult.pageCount });" aria-label="尾页">
								<span aria-hidden="true">&raquo;</span>
							</a>
						</li>
					</c:otherwise>
				</c:choose>
			</ul>
		</nav>
	</div>
</div>