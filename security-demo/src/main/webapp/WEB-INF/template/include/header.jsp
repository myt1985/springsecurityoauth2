<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<nav class="navbar navbar-inverse bg-inverse navbar-toggleable-md">
	<div class="container">
        <a class="navbar-brand" href="<%=application.getContextPath() %>/">用户管理系统</a>
        <sec:authorize access="isAuthenticated()">
        	<div class="row" >
				<form action="<%=application.getContextPath() %>/logout" method="POST" style="height: 100%;">
					<input class="btn btn-success " type="submit" value=" 退 出 " 
						style="margin-top: 8px;float: right;margin-right: 20px;" />
					<%-- CSRF --%>
					<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
				</form>
			</div>
        </sec:authorize>
    </div>
</nav>