$(function() {

	// 获取项目路径
	var contextPath = $("meta[name='_basePath']").attr("content");
	// 手机号正则验证
	var mobileRegex = /^1[0-9]{10}$/g;

	// CSRF
	var csrfToken = $("meta[name='_csrf']").attr("content");
	var csrfHeader = $("meta[name='_csrf_header']").attr("content");
	$.ajaxSetup({
		beforeSend: function(request) {
			if(csrfToken) {
				request.setRequestHeader(csrfHeader, csrfToken); // 添加 CSRF Token
			}
		},
        complete: function (XMLHttpRequest, textStatus) {
			// 解析响应消息
			var json = toJson(XMLHttpRequest.responseText);
			// 权限不足
			if(XMLHttpRequest.status == 403) {
				toastr.error(json.data.exception);
			} else if(XMLHttpRequest.status == 401) { // 登录超时
				toastr.error(json.data.exception);
				toLogin();
			}
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
        	if(XMLHttpRequest.status != 500) {
        		return;
        	}
        	// 解析响应消息
			var data = toJson(XMLHttpRequest.responseText);
			alertMessage(data.data.exception, '知道了', function() {});
        }
	});

	// 消息框全局配置
	if($("script[src*='toastr']")[0]) {
		toastr.options = {
			closeButton: false,
			debug: false,
			progressBar: false,
			positionClass: "toast-top-center",
			onclick: null,
			showDuration: "300",
			hideDuration: "500",
			timeOut: "2000",
			extendedTimeOut: "1000",
			showEasing: "swing",
			hideEasing: "linear",
			showMethod: "fadeIn",
			hideMethod: "fadeOut"
		};
	}

	// 显示异常提示
	if($("#error")[0] && $("#error").val()) {
		toastr.error($("#error").val());
	}

	// 当表单验证插件被引入了，才初始化表单验证插件
	if($("script[src*='Validform']")[0]) {
		// 登录表单验证
		$(".login_form").Validform({
			tiptype: 2,
			showAllError: true
		});
		// 用户添加、编辑表单验证
		$(".user_form").Validform({
			tiptype: 2,
			showAllError: true
		});
	}

	// 当日期插件被引入了，才初始化时间日期插件
	if($("script[src*='laydate']")[0]) {
		// 初始化日期插件
		laydate.render({
			elem: '#birthday',
			type: 'date',
			done: function(value, date, endDate) {
				$("#birthday").val(value).trigger("blur");
			}
		});
		// 初始化时间插件
		laydate.render({
			elem: '#joinTime',
			type: 'datetime',
			done: function(value, date, endDate) {
				$("#joinTime").val(value).trigger("blur");
			}
		});
	}

	// 删除按钮点击事件
	$("[id^='del_']").click(function() {
		var id = this.id.split("_")[1];
		// 确认删除
		layer.confirm("确认是否删除信息？" ,{
			title: false, closeBtn: 0, btn: ['确定','取消']
		}, function(i){
			// 关闭确认框
			layer.close(i);
			// AJAX删除数据
			$.ajax({
				type: "post",
				url: contextPath + "/user/delete/" + id,
				dataType: "json",
				success: function(data) {
					if(data.code == 0) {
						alertMessage(data.message, '知道了', function() {
							window.location.reload();
						});
					} else {
						alertMessage(data.message, '知道了', function() {});
					}
				}
			});
		});
	});
	// 发送短信码
	$("#send_sms").click(function() {
		var mobile = $("#mobile_form input[name='mobile']").val();
		// 手机号验证
		if(!mobileRegex.test(mobile)) {
			alertMessage("请填写手机号码", '知道了', function() {});
			return;
		}
		$.ajax({
			type: "get",
			url: contextPath + "/vCode?type=sms&mobile=" + mobile,
			dataType: "json",
			success: function(data) {
				if(data.code == 0) {
					alertMessage(data.message, '知道了', function() {});
				} else {
					alertMessage(data.message, '知道了', function() {});
				}
			}
		});
	});
	// 短信登录、用户名登录切换
	$(".change-type").click(function() {
		$("#username_form, #mobile_form").toggle();
	});

	// 倒计时
	if($("#timmer")[0]) {
		var t = 5;
		setInterval(function() {
			t--;
			$("#timmer").html(t);
			if(t == 0) {
				window.history.go(-1);
			}
		}, 1000);
	}

	function toLogin() {
		setTimeout(function() {
			window.location.href = contextPath + "/login";
		}, 2000);
	}
	// 切换每页显示数据量
	$("#change-size").change(function() {
		toPage(1);
	});
});
//弹层消息
function alertMessage(message, btn, callback) {
	layer.alert(message, {
	    closeBtn:0,
	    btn: [btn]
	}, function(i) {
		layer.close(i);
		callback();
	});
}
function toJson(text) {
	try {
		var json = JSON.parse(text);
		return json;
	} catch(e) {
		return null;
	}
}
function toPage(page) {
	var size = $("#change-size").val() || 5;
	window.location.href = "users?page=" + page + "&size=" + size;
}
