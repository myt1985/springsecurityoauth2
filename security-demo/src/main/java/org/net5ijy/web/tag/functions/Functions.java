package org.net5ijy.web.tag.functions;

import java.util.List;

/**
 * Net5ijy functions
 *
 * @author 创建人：xuguofeng
 * @version 创建于：2018年9月25日 上午11:42:03
 */
public class Functions {

  /**
   * Tests if an input integer list contains the specified integer.
   *
   * @param list - The input integer list
   * @param target - The specified integer
   * @return true if the list contains the specified integer.
   * @author 创建人：xuguofeng
   */
  public static boolean contains(List<Integer> list, Integer target) {
    return list.contains(target);
  }
}
