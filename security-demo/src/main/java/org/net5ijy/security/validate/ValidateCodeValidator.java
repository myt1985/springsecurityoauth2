package org.net5ijy.security.validate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 验证码验证器
 *
 * @author 创建人：xuguofeng
 * @version 创建于：2018年9月26日 上午8:54:51
 */
public interface ValidateCodeValidator {

  String VALIDATE_TYPE_PARAMETER_NAME = "_type";

  String VALIDATOR_KEY_SUFFIX = "ValidateCodeValidator";

  /**
   * 验证
   *
   * @param request request
   * @param response response
   * @return boolean
   * @author XGF
   * @date 2020/11/30 22:25
   */
  boolean valid(HttpServletRequest request, HttpServletResponse response);
}
