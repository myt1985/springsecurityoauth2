package org.net5ijy.security.validate;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 验证码生成器
 *
 * @author 创建人：xuguofeng
 * @version 创建于：2018年9月20日 上午8:25:36
 */
public interface ValidateCodeGenerator {

  String VALIDATE_GENERATOR_KEY_SUFFIX = "ValidateCodeGenerator";

  /**
   * 生成验证码
   *
   * @param request request
   * @param response response
   * @return ValidateCode
   * @throws IOException IOException
   * @author XGF
   * @date 2020/11/30 22:23
   */
  ValidateCode generateValidateCode(
      HttpServletRequest request, HttpServletResponse response) throws IOException;

  /**
   * 发送验证码
   *
   * @param code code
   * @param request request
   * @param response response
   * @throws IOException IOException
   * @author XGF
   * @date 2020/11/30 22:23
   */
  void sendValidateCode(
      ValidateCode code, HttpServletRequest request, HttpServletResponse response)
      throws IOException;
}
