package org.net5ijy.security.validate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.net5ijy.security.util.exception.ValidateCodeException;
import org.springframework.stereotype.Component;

/**
 * SmsValidateCodeValidator
 *
 * @author XGF
 * @date 2020/11/30 22:22
 */
@Component("smsValidateCodeValidator")
public class SmsValidateCodeValidator implements ValidateCodeValidator {

  public static final String SMS_CODE_PARAMETER_NAME = "sms_code";

  @Override
  public boolean valid(HttpServletRequest request,
      HttpServletResponse response) {

    HttpSession session = request.getSession();

    // 从session获取存入的验证码
    SmsValidateCode sessionCode = (SmsValidateCode) session
        .getAttribute(SmsValidateCodeGenerator.SMS_VALIDATE_CODE_SESSION_KEY);

    // 删除session中的数据
    session.removeAttribute(SmsValidateCodeGenerator.SMS_VALIDATE_CODE_SESSION_KEY);
    // 获取请求中的验证码参数
    String code = request.getParameter(SMS_CODE_PARAMETER_NAME);

    if (code == null) {
      throw new ValidateCodeException("请输入手机验证码");
    }
    if (sessionCode == null) {
      throw new ValidateCodeException("会话手机验证码不存在");
    }
    // 过期
    if (sessionCode.getExpireTime() < System.currentTimeMillis()) {
      throw new ValidateCodeException("手机验证码超时");
    }
    // 手机号不匹配
    if (!sessionCode
        .getMobile()
        .equalsIgnoreCase(
            request.getParameter(SmsValidateCodeGenerator.MOBILE_PARAMETER_NAME))) {
      throw new ValidateCodeException("手机号不匹配");
    }
    // 验证码不正确
    if (!sessionCode.getCode().equalsIgnoreCase(code)) {
      throw new ValidateCodeException("手机验证码不正确");
    }
    return true;
  }
}
