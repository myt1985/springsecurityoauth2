package org.net5ijy.security.validate;

import java.io.Serializable;

/**
 * ValidateCode
 *
 * @author XGF
 * @date 2020/11/30 22:22
 */
public class ValidateCode implements Serializable {

  private static final long serialVersionUID = 2425545609766537291L;

  private String code;

  private long expireIn;

  private long expireTime;

  /**
   * 根据字符串和过期时长创建一个验证码对象，内部会根据expireIn计算出一个对应的expireTime
   */
  public ValidateCode(String code, long expireIn) {
    super();
    this.code = code;
    this.expireIn = expireIn;
    this.expireTime = System.currentTimeMillis() + expireIn;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public long getExpireIn() {
    return expireIn;
  }

  public void setExpireIn(long expireIn) {
    this.expireIn = expireIn;
  }

  public long getExpireTime() {
    return expireTime;
  }

  public void setExpireTime(long expireTime) {
    this.expireTime = expireTime;
  }
}
