package org.net5ijy.security.validate;

/**
 * SmsValidateCode
 *
 * @author XGF
 * @date 2020/11/30 22:22
 */
public class SmsValidateCode extends ValidateCode {

  private static final long serialVersionUID = -158664896598686930L;

  private String mobile;

  public SmsValidateCode(String mobile, String code, long expireIn) {
    super(code, expireIn);
    this.mobile = mobile;
  }

  public String getMobile() {
    return mobile;
  }

  public void setMobile(String mobile) {
    this.mobile = mobile;
  }
}
