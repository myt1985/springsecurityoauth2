package org.net5ijy.security.config.service;

import org.net5ijy.security.bean.User;
import org.net5ijy.security.bean.util.UserState;
import org.net5ijy.security.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

/**
 * MobileUserDetailsService
 *
 * @author XGF
 * @date 2020/11/30 22:36
 */
@Component("mobileUserDetailsService")
public class MobileUserDetailsService implements UserDetailsService {

  private static Logger log = LoggerFactory
      .getLogger(MobileUserDetailsService.class);

  @Autowired
  private UserService userService;

  @Override
  public UserDetails loadUserByUsername(String username)
      throws UsernameNotFoundException {
    User user = userService.getUserByMobile(username);
    if (user == null) {
      log.error("Username not found: " + username);
      throw new UsernameNotFoundException("Username not found: "
          + username);
    }
    return new org.springframework.security.core.userdetails.User(
        user.getUsername(), user.getPassword(), user.getState().equals(
        UserState.ACTIVE.getState()), true, true, true,
        CustomUserDetailsService.getGrantedAuthorities(user));
  }
}
