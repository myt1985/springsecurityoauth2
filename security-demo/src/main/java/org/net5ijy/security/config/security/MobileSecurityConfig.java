package org.net5ijy.security.config.security;

import org.net5ijy.security.web.filter.MobileAuthenticationFilter;
import org.net5ijy.security.web.filter.MobileAuthenticationProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.stereotype.Component;

/**
 * MobileSecurityConfig
 *
 * @author XGF
 * @date 2020/11/30 22:31
 */
@Component
public class MobileSecurityConfig extends
    SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity>
    implements SecurityConstant {

  @Autowired
  @Qualifier("mobileUserDetailsService")
  private UserDetailsService mobileUserDetailsService;

  @Autowired
  @Qualifier("customUserDetailsService")
  private UserDetailsService userDetailsService;

  @Autowired
  private PersistentTokenRepository tokenRepository;

  @Autowired
  private AuthenticationFailureHandler authenticationFailureHandler;

  @Override
  public void configure(HttpSecurity http) throws Exception {
    // 手机号登录认证
    MobileAuthenticationProvider mobileAuthenticationProvider = new MobileAuthenticationProvider();
    mobileAuthenticationProvider
        .setUserDetailsService(mobileUserDetailsService);
    MobileAuthenticationFilter mobileAuthenticationFilter = new MobileAuthenticationFilter();
    mobileAuthenticationFilter.setAuthenticationManager(http
        .getSharedObject(AuthenticationManager.class));
    mobileAuthenticationFilter
        .setAuthenticationFailureHandler(authenticationFailureHandler);
    mobileAuthenticationFilter
        .setRememberMeServices(getPersistentTokenBasedRememberMeServices());
    http.authenticationProvider(mobileAuthenticationProvider)
        .addFilterAfter(mobileAuthenticationFilter,
            UsernamePasswordAuthenticationFilter.class);
  }

  private PersistentTokenBasedRememberMeServices getPersistentTokenBasedRememberMeServices() {
    PersistentTokenBasedRememberMeServices tokenBasedService = new PersistentTokenBasedRememberMeServices(
        FORM_REMEMBER_ME_NAME, userDetailsService, tokenRepository);
    tokenBasedService
        .setTokenValiditySeconds(REMEMBERME_TOKEN_VALID_SECONDS);
    return tokenBasedService;
  }
}
