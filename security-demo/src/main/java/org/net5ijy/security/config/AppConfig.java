package org.net5ijy.security.config;

import org.net5ijy.security.config.converter.DateConverter;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

/**
 * AppConfig
 *
 * @author XGF
 * @date 2020/11/30 22:28
 */
@EnableWebMvc
// @EnableRedisHttpSession(maxInactiveIntervalInSeconds = 120)
@EnableWebSecurity
@EnableTransactionManagement
@Configuration
@ComponentScan({"org.net5ijy.security"})
public class AppConfig extends WebMvcConfigurerAdapter {

  @Override
  public void configureViewResolvers(ViewResolverRegistry registry) {
    InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
    viewResolver.setViewClass(JstlView.class);
    viewResolver.setPrefix("/WEB-INF/template/");
    viewResolver.setSuffix(".jsp");
    registry.viewResolver(viewResolver);
  }

  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    // 静态资源 /static/** 映射到 /static/ 目录
    registry
        .addResourceHandler("/static/**")
        .addResourceLocations("/static/");
  }

  @Override
  public void addFormatters(FormatterRegistry registry) {
    // 添加时间日期格式转换器
    registry.addConverter(new DateConverter());
  }
}
