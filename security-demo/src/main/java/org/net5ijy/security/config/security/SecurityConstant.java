package org.net5ijy.security.config.security;

/**
 * SecurityConstant
 *
 * @author XGF
 * @date 2020/11/30 22:35
 */
public interface SecurityConstant {

  String ROLE_PREFIX = "ROLE_";

  String ANONYMOUS_USER = "ANONYMOUSUSER";

  String FORM_USERNAME_NAME = "username";

  String FORM_PASSWORD_NAME = "password";

  String FORM_REMEMBER_ME_NAME = "remember-me";

  String STATIC_URL = "/static/**";

  String LOGIN_PAGE = "/login";

  String LOGIN_PROCESSING_URL = "/login";

  String MOBILE_LOGIN_PROCESSING_URL = "/mobile_login";

  String LOGOUT_PAGE = "/logout";

  String LOGIN_ERROR_URL = "/login-error";

  String ACCESS_DENIED_FORWARD = "/Access_Denied";

  String V_CODE_URL = "/vCode";

  int REMEMBERME_TOKEN_VALID_SECONDS = 60 * 60 * 24;
}
