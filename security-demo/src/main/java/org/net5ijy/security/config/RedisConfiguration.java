package org.net5ijy.security.config;

import java.util.Arrays;

import javax.servlet.Filter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.session.data.redis.config.annotation.web.http.RedisHttpSessionConfiguration;
import org.springframework.web.context.WebApplicationContext;

import redis.clients.jedis.JedisPoolConfig;

/**
 * RedisConfiguration
 *
 * @author XGF
 * @date 2020/11/30 22:30
 */
@Configuration
@PropertySource(value = {"classpath:application.properties"})
public class RedisConfiguration {

  /**
   * 注入properties配置环境
   */
  @Autowired
  private Environment environment;

  @Autowired
  private WebApplicationContext wac;

  @Bean(name = "jedisPoolConfig")
  public JedisPoolConfig getJedisPoolConfig() {
    JedisPoolConfig poolConfig = new JedisPoolConfig();
    int maxTotal = getIntEnv("redis.pool.maxTotal", 8);
    int maxIdle = getIntEnv("redis.pool.maxIdle", 8);
    poolConfig.setMaxTotal(maxTotal);
    poolConfig.setMaxIdle(maxIdle);
    return poolConfig;
  }

  @Bean(name = "jedisConnectionFactory", destroyMethod = "destroy")
  @Autowired
  public JedisConnectionFactory getJedisConnectionFactory(
      JedisPoolConfig poolConfig) {

    JedisConnectionFactory factory = new JedisConnectionFactory();

    String host = getEnv("redis.hostName", "localhost");
    int port = getIntEnv("redis.port", 6379);
    int timeout = getIntEnv("redis.timeout", 1200);
    boolean usePool = getBoolEnv("redis.usePool", true);

    factory.setHostName(host);
    factory.setPort(port);
    factory.setTimeout(timeout);
    factory.setUsePool(usePool);

    factory.setPoolConfig(poolConfig);

    factory.setPassword(getEnv("redis.password", ""));

    return factory;
  }

  @Bean(name = "redisHttpSessionConfiguration")
  public RedisHttpSessionConfiguration getRedisHttpSessionConfiguration() {

    RedisHttpSessionConfiguration c = new RedisHttpSessionConfiguration();

    int t = getIntEnv(
        "redis.http.session.configuration.maxInactiveIntervalInSeconds",
        1800);

    c.setMaxInactiveIntervalInSeconds(t);

    // [springSessionRepositoryFilter, springSecurityFilterChain]
    System.out.println(Arrays.toString(wac
        .getBeanNamesForType(Filter.class)));
    System.out.println(wac.getBean("springSecurityFilterChain"));

    return c;
  }

  private int getIntEnv(String key, int def) {
    return environment.getProperty(key, int.class, def);
  }

  private String getEnv(String key, String def) {
    return environment.getProperty(key, def);
  }

  private boolean getBoolEnv(String key, boolean def) {
    return environment.getProperty(key, boolean.class, def);
  }
}
