package org.net5ijy.security.config.security;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * SpringSecurityInitializer
 *
 * @author XGF
 * @date 2020/11/30 22:35
 */
public class SpringSecurityInitializer extends AbstractSecurityWebApplicationInitializer {

}
