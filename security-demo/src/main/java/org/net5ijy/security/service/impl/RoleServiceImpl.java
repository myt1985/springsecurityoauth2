package org.net5ijy.security.service.impl;

import java.util.List;

import org.net5ijy.security.bean.Role;
import org.net5ijy.security.dao.RoleDao;
import org.net5ijy.security.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * RoleServiceImpl
 *
 * @author XGF
 * @date 2020/11/30 22:45
 */
@Service
@Transactional(rollbackFor = RuntimeException.class)
public class RoleServiceImpl implements RoleService {

  @Autowired
  private RoleDao roleDao;

  @Override
  public boolean saveRole(Role role) {
    this.roleDao.saveRole(role);
    return true;
  }

  @Override
  public boolean deleteRole(Integer id) {
    this.roleDao.deleteRole(id);
    return true;
  }

  @Override
  public boolean updateRole(Role role) {
    this.roleDao.updateRole(role);
    return true;
  }

  @Override
  @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
  public Role getRole(Integer id) {
    return this.roleDao.getRole(id);
  }

  @Override
  @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
  public List<Role> getRoles() {
    return this.roleDao.getRoles();
  }
}
