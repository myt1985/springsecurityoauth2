package org.net5ijy.security.service;

import java.util.List;

import org.net5ijy.security.bean.Role;

/**
 * RoleService
 *
 * @author XGF
 * @date 2020/11/30 22:44
 */
public interface RoleService {

  /**
   * saveRole
   *
   * @param role role
   * @return boolean
   * @author XGF
   * @date 2020/11/30 22:44
   */
  boolean saveRole(Role role);

  /**
   * deleteRole
   *
   * @param id id
   * @return boolean
   * @author XGF
   * @date 2020/11/30 22:44
   */
  boolean deleteRole(Integer id);

  /**
   * updateRole
   *
   * @param role role
   * @return boolean
   * @author XGF
   * @date 2020/11/30 22:44
   */
  boolean updateRole(Role role);

  /**
   * getRole
   *
   * @param id id
   * @return Role
   * @author XGF
   * @date 2020/11/30 22:44
   */
  Role getRole(Integer id);

  /**
   * getRoles
   *
   * @return Roles
   * @author XGF
   * @date 2020/11/30 22:44
   */
  List<Role> getRoles();
}
