package org.net5ijy.security.service;

import java.util.List;

import org.net5ijy.security.bean.User;
import org.net5ijy.security.util.PageResult;

/**
 * UserService
 *
 * @author XGF
 * @date 2020/11/30 22:45
 */
public interface UserService {

  boolean saveUser(User user);

  boolean deleteUser(Integer id);

  boolean updateUser(User user);

  User getUser(Integer id);

  User getUser(String username);

  User getUserByMobile(String mobile);

  List<User> getUsers();

  List<User> getUsers(int page, int size);

  PageResult<User> getPageUsers(int page, int size);
}
