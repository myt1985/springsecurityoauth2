package org.net5ijy.security.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * PersistentLogin
 *
 * @author XGF
 * @date 2020/11/30 22:27
 */
@Entity
@Table(name = "security_persistent_logins")
public class PersistentLogin implements Serializable {

  private static final long serialVersionUID = -1798600392185391085L;

  @Id
  private String series;

  @Column(name = "username", unique = true, nullable = false)
  private String username;

  @Column(name = "token", unique = true, nullable = false)
  private String token;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "last_used")
  private Date lastUsed;

  public String getSeries() {
    return series;
  }

  public void setSeries(String series) {
    this.series = series;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public Date getLastUsed() {
    return lastUsed;
  }

  public void setLastUsed(Date lastUsed) {
    this.lastUsed = lastUsed;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((series == null) ? 0 : series.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    PersistentLogin other = (PersistentLogin) obj;
    if (series == null) {
      return other.series == null;
    } else {
      return series.equals(other.series);
    }
  }
}
