package org.net5ijy.security.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.net5ijy.security.bean.util.UserState;

/**
 * User
 *
 * @author XGF
 * @date 2020/11/30 22:27
 */
@Entity
@Table(name = "security_user")
public class User implements Serializable {

  private static final long serialVersionUID = -7517808392868672606L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @Column(nullable = false, length = 50, unique = true)
  @NotEmpty(message = "用户名不可以为空")
  @NotNull(message = "用户名不可以为空")
  @Length(min = 6, max = 50, message = "用户名为6至20长度的字母、数字、下划线的组合")
  private String username;

  @Column(nullable = false, length = 150)
  @NotEmpty(message = "登录密码不可以为空")
  @NotNull(message = "登录密码不可以为空")
  @Length(min = 6, max = 150, message = "登录密码为6至20长度的字母、数字、下划线的组合")
  private String password;

  @Column(length = 11, unique = true)
  @NotEmpty(message = "手机号不可以为空")
  @NotNull(message = "手机号不可以为空")
  @Length(min = 11, max = 11, message = "手机号格式错误")
  private String mobile;

  @Column(length = 100)
  @NotEmpty(message = "邮箱不可以为空")
  @NotNull(message = "邮箱不可以为空")
  @Length(min = 6, max = 100, message = "邮箱需要在6至100个字符之间")
  private String email;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "join_time")
  @NotNull(message = "加入时间不可以为空")
  @Past(message = "加入时间需要在当前时间之前")
  private Date joinTime;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "create_time")
  private Date createTime = new Date();

  @Temporal(TemporalType.DATE)
  @NotNull(message = "出生日期不可以为空")
  @Past(message = "出生日期需要在当前时间之前")
  private Date birthday;

  @Column(length = 10)
  private String state = UserState.ACTIVE.getState();

  @ManyToMany(fetch = FetchType.EAGER)
  @JoinTable(name = "security_user_role", joinColumns = {
      @JoinColumn(name = "user_id")}, inverseJoinColumns = {@JoinColumn(name = "role_id")})
  private Set<Role> roles = new HashSet<>();

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getMobile() {
    return mobile;
  }

  public void setMobile(String mobile) {
    this.mobile = mobile;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public Date getJoinTime() {
    return joinTime;
  }

  public void setJoinTime(Date joinTime) {
    this.joinTime = joinTime;
  }

  public Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }

  public Date getBirthday() {
    return birthday;
  }

  public void setBirthday(Date birthday) {
    this.birthday = birthday;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public Set<Role> getRoles() {
    return roles;
  }

  public void setRoles(Set<Role> roles) {
    this.roles = roles;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    User other = (User) obj;
    if (id == null) {
      return other.id == null;
    } else {
      return id.equals(other.id);
    }
  }
}
