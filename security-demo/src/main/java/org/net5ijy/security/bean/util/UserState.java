package org.net5ijy.security.bean.util;

/**
 * UserState
 *
 * @author XGF
 * @date 2020/11/30 22:26
 */
public enum UserState {

  /**
   * ACTIVE
   */
  ACTIVE("Active"),
  /**
   * INACTIVE
   */
  INACTIVE("Inactive"),
  /**
   * DELETED
   */
  DELETED("Deleted"),
  /**
   * LOCKED
   */
  LOCKED("Locked");

  private String state;

  UserState(final String state) {
    this.state = state;
  }

  public String getState() {
    return this.state;
  }

  @Override
  public String toString() {
    return this.state;
  }

  public String getName() {
    return this.name();
  }
}
