package org.net5ijy.security.util;

import java.util.HashMap;
import java.util.Map;

/**
 * ResponseMessage
 *
 * @author XGF
 * @date 2020/11/30 22:52
 */
public class ResponseMessage {

  public static final String MESSAGE_SUCCESS = "操作成功";

  public static final String MESSAGE_ERROR = "操作失败";

  public static final String EXCEPTION_ATTR_KEY = "exception";

  public static final int MESSAGE_SUCCESS_CODE = 0;

  public static final int MESSAGE_ERROR_CODE = 99;

  private int code;

  private String message;

  private Map<String, Object> data = new HashMap<>();

  public ResponseMessage(int code, String message) {
    super();
    this.code = code;
    this.message = message;
  }

  public static ResponseMessage success() {
    return new ResponseMessage(MESSAGE_SUCCESS_CODE, MESSAGE_SUCCESS);
  }

  public static ResponseMessage error() {
    return new ResponseMessage(MESSAGE_ERROR_CODE, MESSAGE_ERROR);
  }

  public ResponseMessage addAttribute(String name, Object val) {
    this.data.put(name, val);
    return this;
  }

  public int getCode() {
    return code;
  }

  public void setCode(int code) {
    this.code = code;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public Map<String, Object> getData() {
    return data;
  }

  public void setData(Map<String, Object> data) {
    this.data = data;
  }
}
