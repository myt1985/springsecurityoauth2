package org.net5ijy.security.util.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * UserNotFoundException
 *
 * @author XGF
 * @date 2020/11/30 22:58
 */
@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "用户不存在")
public class UserNotFoundException extends RuntimeException {

  public UserNotFoundException() {
    super("用户不存在");
  }

  public UserNotFoundException(Integer id) {
    super("用户不存在: " + id);
  }
}
