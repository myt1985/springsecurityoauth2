package org.net5ijy.security.util.exception;

/**
 * UserMobileExistException
 *
 * @author XGF
 * @date 2020/11/30 22:57
 */
public class UserMobileExistException extends RuntimeException {

  public UserMobileExistException() {
    super("手机号已存在");
  }

  public UserMobileExistException(String mobile) {
    super("手机号已存在: " + mobile);
  }
}
