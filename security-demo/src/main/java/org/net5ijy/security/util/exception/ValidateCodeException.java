package org.net5ijy.security.util.exception;

import org.springframework.security.core.AuthenticationException;

/**
 * ValidateCodeException
 *
 * @author XGF
 * @date 2020/11/30 22:58
 */
public class ValidateCodeException extends AuthenticationException {

  public ValidateCodeException(String msg) {
    super(msg);
  }
}
