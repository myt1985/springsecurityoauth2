package org.net5ijy.security.util.exception;

/**
 * UsernameExistException
 *
 * @author XGF
 * @date 2020/11/30 22:57
 */
public class UsernameExistException extends RuntimeException {

  public UsernameExistException() {
    super("用户名已存在");
  }

  public UsernameExistException(String username) {
    super("用户名已存在: " + username);
  }
}
