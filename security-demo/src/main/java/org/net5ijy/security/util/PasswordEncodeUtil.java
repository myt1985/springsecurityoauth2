package org.net5ijy.security.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * PasswordEncodeUtil
 *
 * @author XGF
 * @date 2020/11/30 22:51
 */
public class PasswordEncodeUtil {

  private static PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

  public static String passwordEncode(String password) {
    return passwordEncoder.encode(password);
  }
}
