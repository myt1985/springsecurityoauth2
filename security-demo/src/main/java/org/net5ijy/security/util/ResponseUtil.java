package org.net5ijy.security.util;

/**
 * 一些自定义的响应码
 *
 * @author 创建人：xuguofeng
 * @version 创建于：2018年9月17日 下午1:59:54
 */
public class ResponseUtil {

  /**
   * 登录超时响应码，建议使用401
   */
  @Deprecated
  public static final int STATUS_SESSION_TIMEOUT = 900;

  /**
   * 权限不足响应码，建议使用403
   */
  @Deprecated
  public static final int STATUS_PERMISSION_DENY = 901;

  public static final String APPLICATION_JSON_UTF8 = "application/json; charset=utf-8";

  public static final String SESSION_TIMEOUT_MESSAGE = "登录超时，请重新登录";
}
