package org.net5ijy.security;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.net5ijy.security.config.AppConfig;
import org.net5ijy.security.util.ApplicationSourceUtil;
import org.net5ijy.security.web.listener.CustomHttpSessionListener;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/**
 * SpringMvcInitializer
 *
 * @author XGF
 * @date 2020/11/30 22:19
 */
public class SpringMvcInitializer extends
    AbstractAnnotationConfigDispatcherServletInitializer {

  @Override
  protected Class<?>[] getRootConfigClasses() {
    return new Class[]{AppConfig.class};
  }

  @Override
  protected Class<?>[] getServletConfigClasses() {
    return null;
  }

  @Override
  protected String[] getServletMappings() {
    return new String[]{"/"};
  }

  @Override
  public void onStartup(ServletContext servletContext)
      throws ServletException {

    super.onStartup(servletContext);

    // 获取session活动时长
    ApplicationSourceUtil util = ApplicationSourceUtil.getApplicationSourceUtil();
    Integer sessionSeconds = util.getInteger(
        "net5ijy.security.session.maxInactiveSeconds",
        CustomHttpSessionListener.SESSION_MAX_INACTIVE_INTERNAL);
    // 创建session监听器
    CustomHttpSessionListener sessionListener = new CustomHttpSessionListener();
    sessionListener.setMaxInactiveInternal(sessionSeconds);
    // 注册监听器
    servletContext.addListener(sessionListener);
  }
}
