package org.net5ijy.security.dao;

import java.util.List;

import org.net5ijy.security.bean.Role;

/**
 * RoleDao
 *
 * @author XGF
 * @date 2020/11/30 22:39
 */
public interface RoleDao {

  /**
   * saveRole
   *
   * @param role role
   * @author XGF
   * @date 2020/11/30 22:39
   */
  void saveRole(Role role);

  /**
   * deleteRole
   *
   * @param id id
   * @author XGF
   * @date 2020/11/30 22:39
   */
  void deleteRole(Integer id);

  /**
   * updateRole
   *
   * @param role role
   * @author XGF
   * @date 2020/11/30 22:39
   */
  void updateRole(Role role);

  /**
   * getRole
   *
   * @param id id
   * @return role
   * @author XGF
   * @date 2020/11/30 22:39
   */
  Role getRole(Integer id);

  /**
   * getRoles
   *
   * @return List<Role>
   * @author XGF
   * @date 2020/11/30 22:39
   */
  List<Role> getRoles();
}
