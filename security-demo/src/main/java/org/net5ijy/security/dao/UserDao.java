package org.net5ijy.security.dao;

import java.util.List;

import org.net5ijy.security.bean.User;

/**
 * UserDao
 *
 * @author XGF
 * @date 2020/11/30 22:40
 */
public interface UserDao {

  /**
   * saveUser
   *
   * @param user user
   * @author XGF
   * @date 2020/11/30 22:40
   */
  void saveUser(User user);

  /**
   * deleteUser
   *
   * @param id id
   * @author XGF
   * @date 2020/11/30 22:40
   */
  void deleteUser(Integer id);

  /**
   * updateUser
   *
   * @param user user
   * @author XGF
   * @date 2020/11/30 22:40
   */
  void updateUser(User user);

  /**
   * getUser
   *
   * @param id id
   * @return user
   * @author XGF
   * @date 2020/11/30 22:40
   */
  User getUser(Integer id);

  /**
   * getUser
   *
   * @param username username
   * @return user
   * @author XGF
   * @date 2020/11/30 22:40
   */
  User getUser(String username);

  /**
   * getUserByMobile
   *
   * @param mobile mobile
   * @return user
   * @author XGF
   * @date 2020/11/30 22:40
   */
  User getUserByMobile(String mobile);

  /**
   * getUsers
   *
   * @return users
   * @author XGF
   * @date 2020/11/30 22:40
   */
  List<User> getUsers();

  /**
   * getUsers
   *
   * @param page page
   * @param size size
   * @return users
   * @author XGF
   * @date 2020/11/30 22:40
   */
  List<User> getUsers(int page, int size);

  /**
   * count
   *
   * @return count
   * @author XGF
   * @date 2020/11/30 22:40
   */
  int count();
}
