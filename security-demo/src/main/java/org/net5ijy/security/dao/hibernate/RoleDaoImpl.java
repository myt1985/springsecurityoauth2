package org.net5ijy.security.dao.hibernate;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.net5ijy.security.bean.Role;
import org.net5ijy.security.dao.RoleDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * RoleDaoImpl
 *
 * @author XGF
 * @date 2020/11/30 22:42
 */
@Repository
public class RoleDaoImpl implements RoleDao {

  @Autowired
  private SessionFactory sessionFactory;

  @Override
  public void saveRole(Role role) {
    this.sessionFactory.getCurrentSession().save(role);
  }

  @Override
  public void deleteRole(Integer id) {
    Session session = this.sessionFactory.getCurrentSession();
    Role r = (Role) session.load(Role.class, id);
    session.delete(r);
  }

  @Override
  public void updateRole(Role role) {
    this.sessionFactory.getCurrentSession().merge(role);
  }

  @Override
  public Role getRole(Integer id) {
    return (Role) this.sessionFactory.getCurrentSession().get(Role.class,
        id);
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Role> getRoles() {
    Query query = this.sessionFactory.getCurrentSession().createQuery(
        "from Role");
    return query.list();
  }
}
