package org.net5ijy.security.web.filter;

import java.util.Collection;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

/**
 * MobileAuthenticationToken
 *
 * @author XGF
 * @date 2020/11/30 22:21
 */
public class MobileAuthenticationToken extends AbstractAuthenticationToken {

  private static final long serialVersionUID = -5000346745213272411L;

  private final Object principal;

  public MobileAuthenticationToken(String mobile) {
    super(null);
    this.principal = mobile;
    setAuthenticated(false);
  }

  public MobileAuthenticationToken(Object principal,
      Collection<? extends GrantedAuthority> authorities) {
    super(authorities);
    this.principal = principal;
    super.setAuthenticated(true);
  }

  @Override
  public Object getCredentials() {
    return null;
  }

  @Override
  public Object getPrincipal() {
    return this.principal;
  }
}
