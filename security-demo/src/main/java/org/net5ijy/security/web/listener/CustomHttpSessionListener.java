package org.net5ijy.security.web.listener;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * CustomHttpSessionListener
 *
 * @author XGF
 * @date 2020/11/30 22:20
 */
public class CustomHttpSessionListener implements HttpSessionListener {

  private static Logger log = LoggerFactory.getLogger(CustomHttpSessionListener.class);

  public static final int SESSION_MAX_INACTIVE_INTERNAL = 60 * 30;

  private int maxInactiveInternal = SESSION_MAX_INACTIVE_INTERNAL;

  @Override
  public void sessionCreated(HttpSessionEvent sessionEvent) {
    HttpSession session = sessionEvent.getSession();
    session.setMaxInactiveInterval(maxInactiveInternal);
    log.info("Session[{}]有效时长为[{}]秒", session.getId(), maxInactiveInternal);
  }

  @Override
  public void sessionDestroyed(HttpSessionEvent sessionEvent) {
    HttpSession session = sessionEvent.getSession();
    log.info(String.format("Session[%s]失效", session.getId()));
  }

  public int getMaxInactiveInternal() {
    return maxInactiveInternal;
  }

  public void setMaxInactiveInternal(int maxInactiveInternal) {
    this.maxInactiveInternal = maxInactiveInternal;
  }
}
