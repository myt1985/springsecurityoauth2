1. 有一个已经创建好的web项目
2. 确保本地已经下载并安装完成了tomcat
3. 点击Run菜单，选择Edit Configurations...
4. 点击+号，添加服务配置
5. 找到Tomcat Server选项，选择Local模式
6. 自定义Name，比如Tomcat8.5
7. 点击Configuare，添加本地Tomcat路径，比如E:\tomcat\apache-tomcat-8.5.38
8. 点击Deployment，添加项目
9. 点击+号
10. 选择需要添加tomcat项目；右侧Application content，可以设置访问路径名
11. 设置完成后，点击右下角Apply
12. 返回Server，点击OK
13. 可以看到编辑器左侧，出现了添加的Tomcat
14. 选择项目，点击运行