package org.net5ijy.oauth2.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * /users/list接口调用测试
 *
 * @author xuguofeng
 * @date 2020/8/18 17:58
 */
public class UserListTest {

  // #!/bin/bash
  //
  // while((1<2))
  // do
  // curl -I http://...
  // sleep 2
  // done

  public static void main(String[] args) {

    String cmd = "D:\\app\\curl-7.64.1-win64\\bin\\curl.exe -I http://localhost:7001/users/list?access_token=628b96da-707d-4116-88fc-5ea8ed2762a1";

    for (int i = 0; i < Integer.MAX_VALUE; i++) {

      Runtime r = Runtime.getRuntime();

      try {

        Process process = r.exec(cmd);

        InputStream in = process.getInputStream();

        BufferedReader reader = new BufferedReader(
            new InputStreamReader(in, "UTF-8"));

        String line = reader.readLine();

        while (line != null) {

          System.out.println(line);

          line = reader.readLine();
        }

      } catch (IOException e1) {
        e1.printStackTrace();
      }

      try {
        Thread.sleep(2000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }
}
