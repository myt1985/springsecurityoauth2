package org.net5ijy.oauth2.client;

import static org.net5ijy.oauth2.client.AuthorizationCodeModeTest.*;
import static org.net5ijy.oauth2.client.base.BaseAuthorizationCodeMode.*;

import java.io.IOException;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.net5ijy.commons.http.response.HtmlResponseHolder;

/**
 * 刷新token客户端
 *
 * @author XGF
 * @date 2020/8/6 20:47
 */
@Slf4j
public class RefreshTokenModeTest {

  public static void main(String[] args) throws IOException {

    // 获取token并请求一次资源
    Map map = authorizationCodeModeTest();
    System.out.println(map);

    String refreshToken = map.get("refresh_token").toString();

    // 刷新token
    Map newToken = refreshToken(entity.getTokenUrl(), refreshToken, entity.getAppId(),
        entity.getSecret(), entity.getScope());

    // 第2次访问资源
    HtmlResponseHolder orderDemoResponse =
        resourceDemo(entity.getResourceDemoUrl(), newToken.get("access_token").toString());
    String orderContent = orderDemoResponse.getContent();
    System.out.println(orderContent);
  }
}
