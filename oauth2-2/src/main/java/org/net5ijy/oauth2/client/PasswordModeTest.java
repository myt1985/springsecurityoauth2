package org.net5ijy.oauth2.client;

import static org.net5ijy.oauth2.client.AuthorizationCodeModeTest.entity;
import static org.net5ijy.oauth2.client.base.BaseAuthorizationCodeMode.refreshToken;
import static org.net5ijy.oauth2.client.base.BaseAuthorizationCodeMode.resourceDemo;
import static org.net5ijy.oauth2.client.base.PasswordMode.token;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.net5ijy.commons.http.response.HtmlResponseHolder;

/**
 * 密码模式客户端测试
 *
 * @author xuguofeng
 * @date 2020/8/6 17:28
 */
@Slf4j
public class PasswordModeTest {

  public static void main(String[] args) throws IOException {

    // 第1步使用密码模式获取token
    HtmlResponseHolder responseHolder =
        token(entity.getTokenUrl(), entity.getUsername(), entity.getPassword(), entity.getAppId(),
            entity.getSecret(), entity.getScope());
    String content = responseHolder.getContent();
    System.out.println(content);
    ObjectMapper mapper = new ObjectMapper();
    Map value = mapper.readValue(content, Map.class);

    String accessToken = value.get("access_token").toString();
    String refreshToken = value.get("refresh_token").toString();

    System.out.println("access_token: " + accessToken);
    System.out.println("refresh_token: " + refreshToken);

    // 第2步访问资源
    HtmlResponseHolder orderDemoResponse = resourceDemo(entity.getResourceDemoUrl(), accessToken);
    String orderContent = orderDemoResponse.getContent();
    System.out.println(orderContent);

    // 刷新token
    Map newToken = refreshToken(entity.getTokenUrl(), refreshToken, entity.getAppId(),
        entity.getSecret(), entity.getScope());

    // 第2次访问资源
    HtmlResponseHolder orderDemoResponse2 =
        resourceDemo(entity.getResourceDemoUrl(), newToken.get("access_token").toString());
    String orderContent2 = orderDemoResponse2.getContent();
    System.out.println(orderContent2);
  }
}
