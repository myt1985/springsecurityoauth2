package org.net5ijy.oauth2.client;

import static org.net5ijy.oauth2.client.base.BaseAuthorizationCodeMode.*;

import java.io.IOException;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.net5ijy.commons.http.response.HtmlResponseHolder;
import org.net5ijy.oauth2.client.base.AuthorizationCodeMode;

/**
 * 授权码模式客户端测试
 *
 * @author xuguofeng
 * @date 2020/8/6 10:16
 */
@Slf4j
public class AuthorizationCodeModeTest {

  static AuthorizationCodeBaseEntity entity = AuthorizationCodeBaseEntity.builder()
      .startUrl(
          "http://localhost:7001/oauth/authorize?response_type=code&client_id=net5ijy&redirect_uri=http://localhost:8080&scope=all")
      .baseUrl("http://localhost:7001")
      .loginUrl("http://localhost:7001/login")
      .authorizeUrl("http://localhost:7001/oauth/authorize")
      .tokenUrl("http://localhost:7001/oauth/token")
      .redirectUrl("http://localhost:8080")
      .resourceDemoUrl("http://localhost:7001/order/demo")
      .username("admin1")
      .password("123456")
      .appId("net5ijy")
      .secret("12345678")
      .scope("all")
      .build();

  public static void main(String[] args) throws IOException {
    Map token = authorizationCodeModeTest();
    System.out.println(token);
  }

  static Map authorizationCodeModeTest() throws IOException {

    // 获取token
    Map token = AuthorizationCodeMode.getToken(entity);
    String accessToken = token.get("access_token").toString();

    // 请求资源
    HtmlResponseHolder orderDemoResponse = resourceDemo(entity.getResourceDemoUrl(), accessToken);
    String orderContent = orderDemoResponse.getContent();
    System.out.println(orderContent);

    return token;
  }
}
