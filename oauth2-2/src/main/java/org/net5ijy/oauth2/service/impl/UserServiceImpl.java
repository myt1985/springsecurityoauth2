package org.net5ijy.oauth2.service.impl;

import java.util.List;

import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.net5ijy.oauth2.entity.User;
import org.net5ijy.oauth2.mapper.UserMapper;
import org.net5ijy.oauth2.service.UserService;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

/**
 * 用户业务层实现
 *
 * @author xuguofeng
 * @date 2019/8/28 12:06
 */
@Slf4j
@Service
public class UserServiceImpl implements UserService {

  @Resource
  private UserMapper userMapper;

  @Resource
  private JdbcTemplate jdbcTemplate;

  @Override
  public User getUser(String username) {
    return userMapper.findByUsername(username);
  }

  @Override
  public List<User> getUsers() {
    return userMapper.findUsers();
  }
}
