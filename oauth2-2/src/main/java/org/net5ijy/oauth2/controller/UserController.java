package org.net5ijy.oauth2.controller;

import java.util.List;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.net5ijy.oauth2.entity.User;
import org.net5ijy.oauth2.response.Response;
import org.net5ijy.oauth2.response.ResponseEnum;
import org.net5ijy.oauth2.service.UserService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户信息接口
 *
 * @author xuguofeng
 * @date 2019/8/28 12:03
 */
@Slf4j
@RestController
@RequestMapping(value = "/users")
public class UserController {

  @Resource
  private UserService userService;

  @RequestMapping(value = "/list")
  @ResponseBody
  public Response list() {
    List<User> users = userService.getUsers();
    log.info(users.toString());
    log.info(users.toString());
    log.info(users.toString());
    log.info(users.toString());

    return new Response(ResponseEnum.SUCCESS).setResponseBody(users);
  }
}
