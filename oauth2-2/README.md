# 界面版OAuth2.0

## 使用MySQL保存数据

使用MySQL保存Spring Security OAuth2.0的主要表：

- clientdetails
- oauth_access_token
- oauth_approvals
- oauth_client_details
- oauth_client_token
- oauth_code
- oauth_refresh_token



以及用户、角色表：

- springcloud_role
- springcloud_user
- springcloud_user_role



具体建表语句见springsecurityoauth2.sql脚本。



## 数据源配置

```yaml
spring:
  datasource:
    url: jdbc:mysql://localhost:3306/security_test
    username: system
    password: 123456
    driver-class-name: com.mysql.jdbc.Driver
    type: org.apache.commons.dbcp2.BasicDataSource
    dbcp2:
      initial-size: 5
      max-total: 25
      max-idle: 10
      min-idle: 5
      max-wait-millis: 10000
      validation-query: SELECT 1
```



## Redis配置

用于保存session会话、授权码和oauth token等数据。

```yaml
spring:
  redis:
    host: gerrit-1
    port: 6379
#    password: 123456
    database: 0
```



## 静态页面

### 用户登录页及接口





### 用户授权页及接口





## 服务打包

使用Maven构建

```shell
mvn clean package dependency:copy-dependencies -DoutputDirectory=target/lib
```



## 授权码模式

### 获取code

http://localhost:7001/oauth/authorize?response_type=code&client_id=net5ijy&redirect_uri=http://localhost:8080&scope=all



由于用户没有登录，所以会跳转到登录页面，使用admin/123456进行用户认证。



之后会跳转到授权页。



用户授权后会重定向到http://localhost:8080并且携带code授权码。



### 获取token

http://localhost:7001/oauth/token

POST请求

Basic Auth: net5ijy/12345678

| 参数         | 值                    |
| ------------ | --------------------- |
| grant_type   | authorization_code    |
| code         | ${code}               |
| redirect_uri | http://localhost:8080 |
| scope        | all                   |



### curl命令获取token

```shell
curl --user net5ijy:12345678 -X POST \
-d "grant_type=authorization_code&scope=all&redirect_uri=http%3a%2f%2flocalhost%3a8080&code=AZMoXe" \
http://192.168.0.10:7001/oauth/token
```



响应示例：

```json
{
  "access_token": "86af3d7b-82b4-424e-96da-13435ec55e93",
  "token_type": "bearer",
  "refresh_token": "8341bbac-6fa2-4000-8599-7b6558a73041",
  "expires_in": 41871,
  "scope": "all"
}
```



### 请求资源

http://localhost:7001/order/demo?access_token=${access_token}

http://localhost:7001/users/list?access_token=${access_token}



## 密码模式

### 获取token

http://localhost:7001/oauth/token

POST请求

Basic Auth: net5ijy/12345678

| 参数       | 值          |
| ---------- | ----------- |
| grant_type | password    |
| username   | ${username} |
| password   | ${password} |
| scope      | all         |



### curl命令获取token

```shell
curl --user net5ijy:12345678 -X POST \
-d "grant_type=password&username=admin1&password=123456&scope=all" \
http://192.168.0.10:7001/oauth/token
```



### 请求资源

http://localhost:7001/order/demo?access_token=${access_token}

http://localhost:7001/users/list?access_token=${access_token}



## refresh token

```shell
curl --user net5ijy:12345678 -X POST -d "grant_type=refresh_token&refresh_token=7325322a-7914-4b30-b174-b3b57e77db6d&scope=all" http://192.168.0.10:7001/oauth/token
```



