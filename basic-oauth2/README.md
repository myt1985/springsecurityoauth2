# 最基础版的OAuth2.0

## 自定义Security登录用户名和密码

```yaml
security:
  user:
    name: admin
    password: 123456
```



## 自定义appid和密码

```yaml
security:
  oauth2:
    client:
      client-id: net5ijy
      client-secret: 123456
      authorized-grant-types: authorization_code,refresh_token,password
```



## 服务端口

```yaml
server:
  port: 7000
```



## 授权码模式

### 获取code

http://localhost:7000/oauth/authorize?response_type=code&client_id=net5ijy&redirect_uri=http://localhost:8080&scope=all



首先会弹出用户认证窗口，使用admin/123456进行用户认证。



之后会跳转到授权页。



用户授权后会重定向到http://localhost:8080并且携带code授权码。



### 获取token

http://localhost:7000/oauth/token

POST请求

Basic Auth: net5ijy/123456

| 参数         | 值                    |
| ------------ | --------------------- |
| grant_type   | authorization_code    |
| code         | ${code}               |
| redirect_uri | http://localhost:8080 |
| scope        | all                   |



### curl命令获取token

```shell
curl --user net5ijy:123456 -X POST -d "grant_type=authorization_code&scope=all&redirect_uri=http%3a%2f%2flocalhost%3a8080&code=MdE3eF" http://localhost:7000/oauth/token
```



响应示例：

```json
{
  "access_token": "86af3d7b-82b4-424e-96da-13435ec55e93",
  "token_type": "bearer",
  "refresh_token": "8341bbac-6fa2-4000-8599-7b6558a73041",
  "expires_in": 41871,
  "scope": "all"
}
```



### 请求资源

http://localhost:7000/order/demo?access_token=${access_token}



### Java客户端

org.net5ijy.oauth2.client.AuthorizationCodeModeTest



1. 模拟浏览器访问，进行登录认证
2. 模拟选择Approve然后提交表单
3. 获取重定向Location解析授权码
4. 获取token
5. 请求资源



## 密码模式

### curl命令获取token

```shell
curl --user net5ijy:123456 -X POST -d "grant_type=password&username=admin&password=123456&scope=all" http://192.168.186.1:7000/oauth/token
```



### 请求资源

http://localhost:7000/order/demo?access_token=${access_token}



### Java客户端

org.net5ijy.oauth2.client.PasswordModeTest



1. 使用密码模式获取token
2. 请求资源



## refresh token

### curl命令刷新token

```shell
curl --user net5ijy:123456 -X POST -d "grant_type=refresh_token&refresh_token=7325322a-7914-4b30-b174-b3b57e77db6d&scope=all" http://192.168.186.1:7000/oauth/token
```



### 请求资源

http://localhost:7000/order/demo?access_token=${access_token}



### Java客户端

org.net5ijy.oauth2.client.RefreshTokenModeTest



1. 使用授权码模式获取一次token并且请求一次受保护资源
2. 使用获得到的refresh token刷新token
3. 使用新的access token再次请求受保护资源



