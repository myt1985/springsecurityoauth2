# 界面版OAuth2.0认证服务器

数据库结构、数据源配置、Redis配置、页面接口同界面版OAuth2.0文档。



## 服务打包

使用Maven构建

```shell
mvn clean package dependency:copy-dependencies -DoutputDirectory=target/lib
```



## 授权码模式

### 获取code

http://localhost:7002/oauth/authorize?response_type=code&client_id=net5ijy&redirect_uri=http://localhost:8080&scope=all



由于用户没有登录，所以会跳转到登录页面，使用admin/123456进行用户认证。



之后会跳转到授权页。



用户授权后会重定向到http://localhost:8080并且携带code授权码。



### 获取token

http://localhost:7002/oauth/token

POST请求

Basic Auth: net5ijy/12345678

| 参数         | 值                    |
| ------------ | --------------------- |
| grant_type   | authorization_code    |
| code         | ${code}               |
| redirect_uri | http://localhost:8080 |
| scope        | all                   |



### curl命令获取token

```shell
curl --user net5ijy:12345678 -X POST \
-d "grant_type=authorization_code&scope=all&redirect_uri=http%3a%2f%2flocalhost%3a8080&code=GxY9Yj" \
http://192.168.0.10:7002/oauth/token
```



响应示例：

```json
{
  "access_token": "86af3d7b-82b4-424e-96da-13435ec55e93",
  "token_type": "bearer",
  "refresh_token": "8341bbac-6fa2-4000-8599-7b6558a73041",
  "expires_in": 41871,
  "scope": "all"
}
```



### 请求资源

http://localhost:7003/order/demo?access_token=${access_token}



## refresh token

```shell
curl --user net5ijy:12345678 -X POST -d "grant_type=refresh_token&refresh_token=7325322a-7914-4b30-b174-b3b57e77db6d&scope=all" http://192.168.0.10:7002/oauth/token
```



