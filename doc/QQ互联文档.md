# 接入指南

## 成为开发者

```
http://wiki.connect.qq.com/%E6%88%90%E4%B8%BA%E5%BC%80%E5%8F%91%E8%80%85
```

![](image/101.jpg)



## 创建应用

```
http://wiki.connect.qq.com/__trashed-2
```



应用接入前，首先需进行申请，获得对应的appid与appkey，以保证后续流程中可正确对网站与用户进行验证与授权。



# 网站应用

## 网站应用接入概述

### 网站应用接入方式

第三方网站主要通过使用“QQ登录”接入QQ互联开放平台。

“QQ登录”是QQ互联开放平台提供给第三方网站的一种服务。

“QQ登录”可以让用户使用QQ帐号在第三方网站上登录，分享内容、同步信息，大大降低了用户注册的门槛。



同时，第三方网站根据用户提供的绑定信息，将用户在网站上发布的内容同步发布到QQ空间的个人动态中，从而借助QQ空间庞大的用户群，使网站的信息能通过好友关系得到进一步的传播，提升网站的访问量和用户数。



### 使用QQ登录效果展示

网站开发者可以在网站首页入口和登录、注册页面放置“QQ登录”标识。

![](image/102.jpg)



用户点击按钮之后弹出QQ登录窗口，在登录窗口中将显示网站Logo标识、网站名称以及首页链接地址。

![](image/103.jpg)



如果用户已登录QQ软件，就不用重复输入帐号密码，可以一键实现快速登录并且可以选择授权允许网站访问自己的相关信息。

![](image/104.jpg)



若用户已在该网站注册，则网站可引导用户进行帐号绑定，下次使用QQ帐号登录时，可以同时看到两个帐号的所有信息。

强烈建议网站允许跳过此步，而让用户在登录成功后，在网站设置中选择性绑定，以降低用户登录门槛，提升体验。

用户授权后成功登录网站，在网站上显示用户登录昵称与QQ头像。

建议网站在首页或顶部显示用户通过QQ帐号的登录状态，使用户体验一致，包括用户昵称、QQ头像。



## 网站开发

### 准备appid和appkey

接入QQ登录前，网站需申请成为开发者，创建应用获得对应的appid与appkey，以保证后续流程中可正确对网站与用户进行验证与授权。



appid：应用的唯一标识。在OAuth2.0认证过程中，appid的值即为oauth\_consumer\_key的值。

appkey：appid对应的密钥，访问用户资源时用来验证应用的合法性。在OAuth2.0认证过程中，appkey的值即为oauth\_consumer\_secret的值。



### 登录图标和脚本

在网站页面上放置“QQ登录”按钮，并为按钮添加前台代码，实现点击按钮即弹出QQ登录对话框。



图标下载：

```
http://wiki.connect.qq.com/%E7%BD%91%E7%AB%99%E5%89%8D%E7%AB%AF%E9%A1%B5%E9%9D%A2%E8%A7%84%E8%8C%83#
```



为了实现上述效果，应该为“QQ登录”按钮图片添加如下前台代码：

```
<img src="QQ登录图标地址" onclick="按钮点击事件" />
```

![](image/qq_login_3.png) 



写一个函数“toLogin()”，该函数通过调用“index.php”中的qq\_login函数来实现将页面跳转到QQ登录页面。

```
<script>
 function toLogin() {
   // 注意这里要重新打开窗口
   // 否则后面跳转到QQ登录
   // 授权页面时会直接缩小当前浏览器的窗口，而不是打开新窗口
   var A = window.open("oauth/index.php","TencentLogin",
                       "width=450,height=320,menubar=0,scrollbars=1,resizable=1,status=1,titlebar=0,toolbar=0,location=1");
 }
</script>
```



为按钮添加“toLogin()”事件：

```
<a href="#" onclick='toLogin()'>
  <img src="img/qq_login.png">
</a>
```



![](image/105.jpg)





### 获取Access Token

通过用户验证登录和授权，获取Access Token，为下一步获取用户的OpenID做准备；同时，Access Token是应用在调用OpenAPI访问和修改用户数据时必须传入的参数。

对于应用而言，需要进行两步：

- 获取Authorization Code
- 通过Authorization Code获取Access Token



#### 获取Authorization Code

PC网站请求地址：https://graph.qq.com/oauth2.0/authorize

请求方法：GET

请求参数：

| 参数          | 必填 | 说明                                                         |
| ------------- | ---- | ------------------------------------------------------------ |
| response_type | Y    | 授权类型，此值固定为“code”                                   |
| client_id     | Y    | 申请QQ登录成功后，分配给应用的appid                          |
| redirect_uri  | Y    | 成功授权后的回调地址，必须是注册appid时填写的主域名下的地址，建议设置为网站首页或网站的用户中心。注意需要将url进行URLEncode |
| state         | Y    | client端的状态值。用于第三方应用防止CSRF攻击，成功授权后回调时会原样带回。请务必严格按照流程检查用户与state参数状态的绑定 |
| scope         | N    |                                                              |
| display       | N    | 仅PC网站接入时使用。                                         |



scope - 请求用户授权时向用户显示的可进行授权的列表。可填写的值是API文档中列出的接口，以及一些动作型的授权（目前仅有：do\_like），如果要填写多个接口名称，请用逗号隔开。例如：scope=get\_user\_info,list\_album,upload\_pic,do\_like。不传则默认请求对接口get\_user\_info进行授权。建议控制授权项的数量，只传入必要的接口名称，因为授权项越多，用户越可能拒绝进行任何授权。



如果用户成功登录并授权，则会跳转到指定的回调地址，并在redirect\_uri地址后带上Authorization Code和原始的state值。

注意：此code会在10分钟内过期。

如果用户在登录授权过程中取消登录流程，对于PC网站，登录页面直接关闭；对于WAP网站，同样跳转回指定的回调地址，并在redirect\_uri地址后带上usercancel参数和原始的state值，其中usercancel值为非零。



#### 获取Access Token

PC网站请求地址：https://graph.qq.com/oauth2.0/token

请求方法：GET

请求参数：

| 参数          | 必填 | 说明                                             |
| ------------- | ---- | ------------------------------------------------ |
| grant_type    | Y    | 授权类型，在本步骤中，此值为“authorization_code” |
| client_id     | Y    | 申请QQ登录成功后，分配给网站的appid              |
| client_secret | Y    | 申请QQ登录成功后，分配给网站的appkey             |
| code          | Y    | 上一步返回的authorization code                   |
| redirect_uri  | Y    | 与上面一步中传入的redirect_uri保持一致           |



如果成功返回，即可在返回包中获取到Access Token信息，如：

```
access_token=FE04************************CCE2&expires_in=7776000&refresh_token=88E4************************BE14
```



#### 刷新Access Token

（可选）

Access\_Token的有效期默认是3个月，过期后需要用户重新授权才能获得新的Access\_Token。本步骤可以实现授权自动续期，避免要求用户再次授权的操作，提升用户体验。

PC网站请求地址：https://graph.qq.com/oauth2.0/token

请求方法：GET

请求参数：

| 参数          | 必填 | 说明                                        |
| ------------- | ---- | ------------------------------------------- |
| grant_type    | Y    | 授权类型，在本步骤中，此值为“refresh_token” |
| client_id     | Y    | 申请QQ登录成功后，分配给网站的appid         |
| client_secret | Y    | 申请QQ登录成功后，分配给网站的appkey        |
| refresh_token | Y    | 在Step2中，返回的refres_token               |



如果成功返回，即可在返回包中获取到Access Token信息。如：

```
access_token=FE04************************CCE2&expires_in=7776000&refresh_token=88E4************************BE14
```



### 获取用户OpenID

通过输入在上一步获取的Access Token，得到对应用户身份的OpenID。

OpenID是此网站上或应用中唯一对应用户身份的标识，网站或应用可将此ID进行存储，便于用户下次登录时辨识其身份，或将其与用户在网站上或应用中的原有账号进行绑定。

PC网站：https://graph.qq.com/oauth2.0/me

GET请求

请求参数：

access_token - 必须，在Step1中获取到的access token。



PC网站接入时，获取到用户OpenID，返回包如下：

```
callback( {"client_id":"YOUR_APPID","openid":"YOUR_OPENID"} );
```



### OpenAPI调用说明

获取到Access Token和OpenID后，可通过调用OpenAPI来获取或修改用户个人信息。

#### 前提说明

- 该appid已经开通了该OpenAPI的使用权限。从API列表的接口列表中可以看到，有的接口是完全开放的，有的接口则需要提前提交申请，以获取访问权限。
- 准备访问的资源是用户授权可访问的。
- 已经成功获取到Access Token，并且Access Token在有效期内。



#### 接口参数说明

QQ登录提供了用户信息/动态同步/日志/相册/微博等OpenAPI（详见API列表），网站需要将请求发送到某个具体的OpenAPI接口，以访问或修改用户数据。

调用所有OpenAPI时，除了各接口私有的参数外，所有OpenAPI都需要传入基于OAuth2.0协议的通用参数：

| 参数                 | 说明                                                         |
| -------------------- | ------------------------------------------------------------ |
| access\_token        | 可通过使用Authorization\_Code获取Access\_Token 或来获取。access\_token有3个月有效期。 |
| oauth\_consumer\_key | 申请QQ登录成功后，分配给应用的appid                          |
| openid               | 用户的ID与QQ号码一一对应。可通过调用https://graph.qq.com/oauth2.0/me?access_token=YOUR_ACCESS_TOKEN来获取。 |



#### 以get\_user\_info接口为例

```
https://graph.qq.com/user/get_user_info?access_token=YOUR_ACCESS_TOKEN&oauth_consumer_key=YOUR_APP_ID&openid=YOUR_OPENID
```

成功返回后，即可获取到用户数据：

```
{
  "ret": 0,
  "msg": "",
  "nickname": "YOUR_NICK_NAME",
  ...
}
```



# API和SDK

## API

### API列表

![](image/106.jpg)



### get\_user\_info

获取登录用户在QQ空间的信息，包括昵称、头像、性别及黄钻信息（包括黄钻等级、是否年费黄钻等）。

此接口主要用于网站使用QQ登录时，直接拉取用户在QQ空间的昵称、头像、性别等信息，降低用户的注册成本。

```
url			  https://graph.qq.com/user/get_user_info
支持验证方式	  oauth2.0
格式			 JSON
http请求方式   GET
是否需要鉴权	  需要
```

参数说明：

| 参数                 | 说明                                                         |
| -------------------- | ------------------------------------------------------------ |
| access\_token        | 可通过使用Authorization\_Code获取Access\_Token 或来获取。access\_token有3个月有效期。 |
| oauth\_consumer\_key | 申请QQ登录成功后，分配给应用的appid                          |
| openid               | 用户的ID与QQ号码一一对应。可通过调用https://graph.qq.com/oauth2.0/me?access_token=YOUR_ACCESS_TOKEN来获取。 |



返回说明：

| 返回值           | 说明                                                         |
| ---------------- | ------------------------------------------------------------ |
| ret              | 返回码                                                       |
| msg              | 如果ret&lt;0，会有相应的错误信息提示，返回数据全部用UTF-8编码 |
| nickname         | 用户在QQ空间的昵称                                           |
| figureurl        | 30×30像素的QQ空间头像URL                                     |
| figureurl\_1     | 50×50像素的QQ空间头像URL                                     |
| figureurl\_2     | 100×100像素的QQ空间头像URL                                   |
| figureurl\_qq\_1 | 40×40像素的QQ头像URL                                         |
| figureurl\_qq\_2 | 100×100像素的QQ头像URL。需要注意，不是所有的用户都拥有QQ的100x100的头像，但40x40像素则是一定会有 |
| gender           | 性别。如果获取不到则默认返回"男"                             |

```
{
  "ret":0,
  "msg":"",
  "nickname":"Peter",
  "figureurl":"http://qzapp.qlogo.cn/EAFBD4DCE2C1FC775E56/30",
  "figureurl_1":"http://qzapp.qlogo.cn/D4DCE2C1FC775E56/50",
  "figureurl_2":"http://qzapp.qlogo.cn/4DCE2C1FC775E56/100",
  "figureurl_qq_1":"http://q.qlogo.cn/4A5422917B6/40",
  "figureurl_qq_2":"http://q.qlogo.cn/7FB4A5422917B6/100",
  "gender":"男",
  "is_yellow_vip":"1",
  "vip":"1",
  "yellow_vip_level":"7",
  "level":"7",
  "is_yellow_year_vip":"1"
}
```



### API调试工具

```
https://connect.qq.com/sdk/webtools/index.html
```



## API返回码





## SDK

### SDK下载

QQ互联平台为广大开发者整理了以下SDK列表，辅助开发者快速接入QQ登录、分享等功能。

使用时请注意：

- 对于非官方SDK包，由于腾讯未对其进行验证，因此无法保证其可用性。使用时遇到问题请直接与作者联系或在论坛上反馈，腾讯公司将不提供相关的技术支持。
- 全部基于OAuth2.0协议，不再支持OAuth1.0协议。



SDK for 移动应用接入。

SDK for 移动游戏接入。

SDK for 网站接入。



### JS\_SDK使用说明

```
http://wiki.connect.qq.com/js_sdk%E4%BD%BF%E7%94%A8%E8%AF%B4%E6%98%8E
```

JS SDK获取地址：

```
https://connect.qq.com/qc_jssdk.js
```

为了让应用更快接入，腾讯提供了JS SDK。JS SDK是现有最简单的接入QQ互联的方式。

JS SDK基于QQ互联OAuth2.0协议的client-side模式，封装了登录流程与API列表中的所有OpenAPI调用方法。开发者不需了解协议，只需要前台交互，以及将相关的参数修改成自身对应的参数即可使用。同时，QQ互联又提供了可供第三方高级需求进行自行配置的可选参数与相关函数，使开发者可以根据自身需求灵活使用。

本JS SDK不需要配置任何跨域文件，支持在绝大多数主流浏览器下使用；对于少数老版本的浏览器，需要浏览器支持flash插件来完成跨域通信的问题。



#### 引用JS SDK的JavaScript文件

在html页面适当的位置引入JS脚本包：

```
<script type="text/javascript" charset="utf-8"
    src="http://connect.qq.com/qc_jssdk.js"
    data-appid="APPID"
    data-redirecturi="REDIRECTURI"></script>
```



#### 放置QQ登录按钮

在html页面需要插入QQ登录按钮的位置：

```
<span id="qqLoginBtn"></span>
<script type="text/javascript">
    QC.Login({
       btnId: "qqLoginBtn" // 插入按钮的节点id
});
</script>
```



上述代码中放置了一个html元素节点，并给该节点指定全页面唯一的id值，如例子中的：

```
<span id="qqLoginBtn"></span>
```



点击该按钮，即可发起登录请求。

JS SDK封装了获取Access Token以及OpenID的方法，因此开发者不需要再开发代码进行获取，直接调用QQ登录OpenAPI即可。



使用QC.Login(options, loginFun, logoutFun, outCallBackFun)进行初始化。

options参数说明：

![](image/107.jpg)



#### 回调地址页面

该步骤的作用是回调地址将获取到的Access Token和OpenID返回给调用页面。

在回调地址页面，即1.1节中回调地址“REDIRECTURI”指定的页面，粘贴如下代码：

```
<script type="text/javascript" src="//connect.qq.com/qc_jssdk.js" charset="utf-8" data-callback="true" />
```



#### 调用QQ登录OpenAPI

QQ互联在JS SDK中封装了所有的OpenAPI接口，开发者只需要传递OpenAPI名称，以及OpenAPI需要的相关参数，就可以调用OpenAPI。

调用OpenAPI时，请统一遵循下述调用方式：

```
QC.api(api, paras, fmt, method)
```

参数说明：

![](image/108.jpg)



每个OpenAPI调用时均指定了一个Request对象，开发者可根据OpenAPI请求完成情况指定不同的处理函数。

每次QC.api调用的异步响应都会返回一个Response对象，用于接收OpenAPI的返回值，包括返回格式、返回数据、OpenAPI请求错误码等。



### JS\_SDK的其他公开方法

![](image/109.jpg)





